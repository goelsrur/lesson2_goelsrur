#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
unordered_map<string, vector<string>> results;
int temp = 0;

void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
		}
	}

	

	temp = stoi(argv[0]);

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg)
{
	bool checkList = true;
	int rc;
	string sql_query = "select available from cars where id = " + to_string(carid);;
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	int avilableCar = temp;
	if (avilableCar == 0)
	{
		checkList = false;
	}

	sql_query = "select price from cars where id = " + to_string(carid);;
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	int priceOfCar = temp;


	sql_query = "select balance from accounts where id =" + to_string(buyerid);;
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	int balanceFromBuyer = temp;

	if (priceOfCar > balanceFromBuyer)
	{
		checkList = false;
	}
	clearTable();

	if (checkList)
	{
		string dBalance = to_string(balanceFromBuyer - priceOfCar); // remains
		sql_query = "update accounts set balance = " + dBalance + " where id = " + to_string(buyerid);
		rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
		sql_query = "update cars set available = 0 where id = " + to_string(carid);
		rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
		cout << "The purchase was made";
	}
	else 
	{
		cout << "you can't buy.";
	}
	return checkList;
}

bool balanceTransfer(int from,int to,int amount,sqlite3* db,char* zErrMsg)
{
	

	int rc;
	string sql_query = "begin transaction";
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);

	sql_query = "select balance from accounts where id = " + to_string(to);
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	int balanceOfTo = temp;

	sql_query = "select balance from accounts where id = " + to_string(from);
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	int balanceOfFrom = temp;
	
	sql_query = "update accounts set balance = " + to_string(balanceOfTo + amount) + " where id = " + to_string(to);
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);

	sql_query = "update accounts set balance = " + to_string(balanceOfFrom - amount) + " where id = " + to_string(from);
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);

	
	sql_query = "commit";
	rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);
	clearTable();
	
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return false;
	}
	else
	{
		sql_query = "select id,balance from accounts where id = " + to_string(to) + " or id = " + to_string(from);
		rc = sqlite3_exec(db, sql_query.c_str(), callback, 0, &zErrMsg);

		printTable();
	}

	return true;
}

int pushIntoVector(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc; i++)
	{
		ids.push_back(argv[i]);
	}
	return 0;
}

int massageReturned(void* notUsed, int argc, char** argv, char** azCol)
{
	for (int i = 0; i < argc;i+=2)
	{
		cout << argv[i] << " - "<<argv[i+1] << endl;
	}
	return 0;
}




int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	rc = sqlite3_open("carsDealer.db", &db);
	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}
	else
	{
		
		
		int buyerId, carId;
		cout << "enter buyer id:";
		cin >> buyerId;
		cout << "enter car id:";
		cin >> carId;
		carPurchase(buyerId, carId, db, zErrMsg);
		

		
		balanceTransfer(1, 2, 1, db, zErrMsg);


		


	}
	
	system("Pause");
	return 0;
}